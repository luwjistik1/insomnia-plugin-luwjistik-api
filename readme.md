# About

A plugin for [Insomnia](https://insomnia.rest/) to automatically injects path and calculate signature. Will search for `LUWJISTIK_API_SECRET_KEY` in the env and use it to calculate the values to inject if found.
