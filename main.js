// For help writing plugins, visit the documentation to get started:
//   https://support.insomnia.rest/article/173-plugins

const crypto = require("crypto");
const u = require('url');

function GenerateSignature(path, clientSecret, payload) {
  const hmac = crypto.createHmac("sha256", clientSecret);
  hmac.write(path + payload);
  hmac.end();
  return hmac.read().toString("base64");
}

module.exports.requestHooks = [
  function(context){
    let body = context.request.getBody().text;
    if(!body){
      body = ''
    }
    let reqUrl = new u.URL(context.request.getUrl());
    let path = reqUrl.pathname;
    let secret = context.request.getEnvironmentVariable('LUWJISTIK_API_SECRET_KEY');
    if(secret){
      let signature = GenerateSignature(path, secret, body);
      context.request.setHeader('Signature', signature)
      context.request.setHeader('Path', path)
    }
  }
]
